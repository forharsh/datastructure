package sorting;

public class SelectionSort {
    static void printArray(int arr[])
    {
        for (int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        int arr[] = {5, 1, 4, 2, 8};

        System.out.println("Given Array");
        printArray(arr);
        SelectionSort selectionSort = new SelectionSort();
        selectionSort.sort(arr);
        printArray(arr);
    }

    private void sort(int[] arr) {
        for(int i = 0; i < arr.length; i++) {
            int min = arr[i];
            for(int j =  i + 1 ;j < arr.length; j++)
                if(min > arr[j]) {
                    int k = arr[j];
                    arr[j] = arr[i];
                    arr[i] = k;
                    min = arr[i];
                }
        }
    }

}
