package sorting;

public class BubbleSort {

    static void printArray(int arr[])
    {
        for (int i : arr)
            System.out.print(i + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        int arr[] = {5, 1, 4, 2, 8};

        System.out.println("Given Array");
        printArray(arr);
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(arr,arr.length);
        printArray(arr);
    }

    public static void sort(int[] arr,int length) {
        if(length == 1){
            return;
        }
        for (int i = 0; i < length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                int k = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = k;
            }
        }
        sort(arr, length-1);
    }
}
