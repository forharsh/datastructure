package sapient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Producer implements Runnable{
    final List queue;
    Producer(final List queue) {
        this.queue = queue;
    }
    @Override
    public void run() {
        System.out.println("Producer Thread");
        int i = 0;
        while (i <= 10){
            producer(i++);
        }
    }

    private void producer(final int q) {
        synchronized (queue) {
            while (queue.size() > 0) {
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized (queue) {
            while (queue.size() == 0) {
                System.out.println("PRODUCED: " + q);
                queue.add(q);
                queue.notify();
            }
        }

    }
}
class Consumer implements Runnable{
    final List queue;
    Consumer(final List hashSet) {
        this.queue = hashSet;
    }
    @Override
    public void run() {
        while (true) {
            consume();
        }
    }

    private void consume() {
        synchronized (queue) {
            while(queue.size() == 0) {
                try {
                    queue.wait();
                    System.out.println("Waiting producer to produce");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized (queue) {
            while(queue.size() > 0)  {
                System.out.println("Consume: " + queue.remove(0));
                queue.notify();
            }
        }
    }
}
public class ProducerConsumerProblem {

    public static void main(String[] args) {
           final List list = new ArrayList();
        final ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(new Producer(list));
        executorService.submit(new Consumer(list));
        executorService.shutdown();
    }
}
