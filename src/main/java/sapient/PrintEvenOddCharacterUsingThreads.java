package sapient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Shared {
    Boolean isOdd = Boolean.FALSE;
}

class PrintOdd implements Runnable {
    Shared shared;

    PrintOdd(Shared shared) {
        this.shared = shared;
    }

    @Override
    public void run() {
        synchronized (shared) {
            while (true) {
                if (shared.isOdd) {
                    System.out.print(" B ");
                    shared.isOdd = false;
                    shared.notify();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        shared.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

class PrintEven implements Runnable {
    Shared shared;

    PrintEven(Shared shared) {
        this.shared = shared;
    }

    @Override
    public void run() {
        synchronized (shared) {
            while (true) {
                if (shared.isOdd) {
                    try {
                        shared.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.print(" A ");
                    shared.isOdd = true;
                    shared.notify();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

public class PrintEvenOddCharacterUsingThreads {

    public static void main(String[] args) {
        Shared shared = new Shared();
        PrintOdd printOdd = new PrintOdd(shared);
        PrintEven printEven = new PrintEven(shared);
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(printEven);
        service.submit(printOdd);
        service.shutdown();
    }
}
