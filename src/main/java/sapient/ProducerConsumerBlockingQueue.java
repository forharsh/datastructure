package sapient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Producer1 implements Runnable{
    final BlockingQueue queue;
    Producer1(final BlockingQueue queue) {
        this.queue = queue;
    }
    @Override
    public void run() {
        System.out.println("Producer Thread");
        int i = 0;
        while (true){
            producer(i++);
        }
    }

    private void producer(final int q) {
        try {
            System.out.println("PRODUCING: " + q);
            queue.put(q);
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
class Consumer1 implements Runnable{
    final BlockingQueue queue;
    Consumer1(final BlockingQueue hashSet) {
        this.queue = hashSet;
    }
    @Override
    public void run() {
        while (true) {
            consume();
        }
    }

    private void consume() {
        System.out.println("CONSUME === " +  queue.poll());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
public class ProducerConsumerBlockingQueue {

        public static void main(String[] args) {
            final BlockingQueue list = new ArrayBlockingQueue(10);
            final ExecutorService executorService = Executors.newFixedThreadPool(2);
            executorService.submit(new Producer1(list));
            executorService.submit(new Consumer1(list));
            executorService.shutdown();
    }
}
