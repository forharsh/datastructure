package sapient;

import java.util.stream.Stream;

class Node {
    int data;
    Node next;

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }
}
public class SingleLinkedList {
    private Node node;
    private Node first;

    public SingleLinkedList() {
        this.node = null;
    }
    public static void main(String[] args) {
        final SingleLinkedList singleLinkedList = new SingleLinkedList();
        singleLinkedList.add(1);
        singleLinkedList.add(2);
        singleLinkedList.add(3);
        singleLinkedList.add(4);
        singleLinkedList.add(5);
        singleLinkedList.print();
        singleLinkedList.reverse();
        singleLinkedList.print();
        new Double(9.9).intValue();
    }

    private void reverse() {
        Node first = node;
        Node prev = null;
        Node current;
        while (first != null) {
            current = first;
            first = first.next;
            current.next = prev;
            prev = current;
        }
        node = prev;
    }

    private void print() {
        Node traverse = node;
        System.out.println();
        while (traverse != null ) {
            System.out.print(traverse.data+ " ==> ");
            traverse = traverse.next;
        }
    }

    private void add(int i) {
        if (node == null) {
            node = new Node(i,null);
            first = node;
        } else {
           Node  node = new Node(i,null);
           first.next = node;
           first = node;
        }
    }
}
