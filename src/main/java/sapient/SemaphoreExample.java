package sapient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreExample {

    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
        for(int i = 1; i <= 200; i++) {
            service.submit(new Runnable() {
                @Override
                public void run() {
                    Connection.getConnection().connect();
                }
            });
        }
        service.shutdown();
    }
}

class Connection {

    private static Connection conn = new Connection();
    private int connection = 0;
    Semaphore semaphore = new Semaphore(10);
    private Connection() {

    }

    public static Connection getConnection() {
        return conn;
    }

    public void connect() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (this) {
            connection++;
            System.out.println("Connection count = " + connection);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (this) {
            connection--;
        }
        semaphore.release();
    }
}
