package sapient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
class SharedResource {
    boolean isEven = true;
    int count = 0;
}
class EvenThread implements Runnable {
    SharedResource sharedResource;
    final String toPrint;
    public EvenThread(final SharedResource sharedResource, final String toPrint) {
        this.sharedResource = sharedResource;
        this.toPrint = toPrint;
    }

    @Override
    public void run() {
        synchronized (sharedResource) {
            while (true) {
                if(sharedResource.count == toPrint.length()) {
                    break;
                }
                if(sharedResource.isEven) {
                    System.out.println(toPrint.charAt(sharedResource.count++) + " thread: " + Thread.currentThread().getName());
                    sharedResource.isEven = false;
                    sharedResource.notify();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        sharedResource.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
class OddThread implements Runnable {
    SharedResource sharedResource;
    final String toPrint;

    public OddThread(SharedResource sharedResource, final String toPrint) {
        this.sharedResource = sharedResource;
        this.toPrint = toPrint;
    }

    @Override
    public void run() {
        synchronized (sharedResource) {
            while (true) {
                if(sharedResource.count == toPrint.length()) {
                    break;
                }
                if(sharedResource.isEven) {
                    try {
                        sharedResource.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println(toPrint.charAt(sharedResource.count++) + " thread: " + Thread.currentThread().getName());
                    sharedResource.isEven = true;
                    sharedResource.notify();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
public class PrintStringUsingThread {

    public static void main(String[] args) {
        final String string = "Hello Sejal!";
        final SharedResource sharedResource = new SharedResource();
        EvenThread evenThread = new EvenThread(sharedResource, string);
        OddThread oddThread = new OddThread(sharedResource, string);
        final ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(evenThread);
        executorService.submit(oddThread);
        executorService.shutdown();
    }
}
