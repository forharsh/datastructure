package sapient;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Deadlock implements Runnable {

    @Override
    public void run() {
        for(int i = 1; i < 500 ; i++) {
            System.out.println(i);
        }
       /* synchronized (this) {
            if("Thread-1".equals(Thread.currentThread().getName())) {
                try {
                    method1();
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    method2();
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }*/
    }

    private synchronized void method1() {
        method2();
    }

    private synchronized void method2() {
        method1();
    }

    public static void main(String[] args) {
        final Deadlock deadlock1 = new Deadlock();
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Thread t1 = new Thread(deadlock1,"Thread-1");
        Thread t2 = new Thread(deadlock1,"Thread-2");
        executorService.submit(t1);
        executorService.submit(t2);
        try {
            t1.join();
            t2.join();
            System.out.println("Now main thread ends." + Thread.currentThread().isDaemon());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();

        Map hashMap = new WeakHashMap();
        String str = "hello";
        hashMap.put("A","B");
        str = null;
        System.out.println(hashMap);
    }
}
