package sapient;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class Foo {
    String name;
    List<Bar> bars = new ArrayList<>();

    Foo(String name) {
        this.name = name;
    }
}

class Bar {
    String name;

    Bar(String name) {
        this.name = name;
    }
}
public class java8 {
    public static void main(String[] args) {
        List<Foo> foos = new ArrayList<>();
// create foos
        IntStream
                .range(1, 4)
                .forEach(i -> foos.add(new Foo("Foo" + i)));

// create bars
        foos.forEach(f ->
                IntStream
                        .range(1, 4)
                        .forEach(i -> f.bars.add(new Bar("Bar" + i + " <- " + f.name))));

        foos.stream().flatMap(foo -> foo.bars.stream()).forEach(bar -> {System.out.println(bar.name);});

        Stream.of("a1","b2").reduce((str,str2) -> str+=str2).ifPresent(System.out::println);
        foos.stream().reduce((foo1,foo2) -> {
            foo1.name = foo1.name + "--" + foo2.name;
            return foo1;
        }).ifPresent(System.out::println);

    }
}
