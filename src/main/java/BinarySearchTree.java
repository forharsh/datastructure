import java.util.LinkedList;
import java.util.Queue;

class Node {
    Integer data;
    Node left, right;

    public Node(final Integer data, final Node left, final Node right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }
}

public class BinarySearchTree {

    Node rootNode;

    BinarySearchTree() {
        rootNode = null;
    }

    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree();
        tree.addNode(50);
        tree.addNode(30);
        tree.addNode(20);
        tree.addNode(40);
        tree.addNode(70);
        tree.addNode(60);
        tree.addNode(80);

        tree.preOrderTraversal(tree.rootNode);
        tree.postOrderTraversal(tree.rootNode);
        System.out.println();
        System.out.println("Is Binary Tree: " + tree.isBinaryTree(tree.rootNode));
        System.out.println("Height of tree: " + tree.findHeight(tree.rootNode));
        tree.levelOrderTraversal(tree.rootNode);
    }

    public void addNode(Integer data) {
        rootNode = add(rootNode, data);
    }

    public Node add(Node rootNode, int data) {
        if (rootNode == null) {
            Node node = new Node(data, null, null);
            rootNode = node;
        } else {
            if (data < rootNode.data) {
                rootNode.left = add(rootNode.left, data);
            } else {
                rootNode.right = add(rootNode.right, data);
            }
        }
        return rootNode;
    }

    private void levelOrderTraversal(Node rootNode) {
        Node node = rootNode;
        if (node == null) {
            return;
        }
        System.out.println("Level Order Traversal");
        Queue<Node> queue = new LinkedList<>();
        queue.add(node);
        while (!queue.isEmpty()) {
            Node node1 = queue.remove();
            if (node1.left != null) {
                queue.add(node1.left);
            }
            if (node1.right != null) {
                queue.add(node1.right);
            }
            System.out.print(node1.data + " ");
        }
    }

    private int findHeight(Node node) {
        if (node == null) {
            return -1;
        }
        return Math.max(findHeight(node.left), findHeight(node.right)) + 1;
    }

    private void preOrderTraversal(Node node) {
        if (node == null) {
            return;
        }
        System.out.print(node.data + " ");
        preOrderTraversal(node.left);
        preOrderTraversal(node.right);
    }

    private void postOrderTraversal(Node node) {
        if (node == null) {
            return;
        }

        postOrderTraversal(node.left);
        System.out.print(node.data + " ");
        postOrderTraversal(node.right);
    }

    public boolean isBinaryTree(Node node) {
        boolean binaryTree = false;
        if (node == null) {
            return true;
        } else {
            if (node.data >= Integer.MIN_VALUE && node.data <= Integer.MAX_VALUE && isBinaryTree(node.left) && isBinaryTree(node.right)) {
                binaryTree = true;
            }
        }
        return binaryTree;
    }
}
