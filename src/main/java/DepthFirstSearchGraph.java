import java.util.Iterator;
import java.util.LinkedList;

public class DepthFirstSearchGraph {
    int size;
    private LinkedList arrLinkedList[];
    private boolean visitedArray[];
    public DepthFirstSearchGraph(int size){
        this.size = size;
        arrLinkedList = new LinkedList[size];
        visitedArray = new boolean[size];
        for(int i = 0 ; i < size ; i++) {
            arrLinkedList[i] = new LinkedList();
        }
    }


    public void addEdge(int src, int dest){
        arrLinkedList[src].add(dest);
      //  arrLinkedList[dest].add(src);
    }

    public void printEdge() {
        int vertex = 0;
        for (LinkedList<Integer> linklist: arrLinkedList) {
            System.out.print("in vertex " + vertex++);
            for (Integer value: linklist ) {
                System.out.print("--> " + value);
            }
            System.out.println();
        }
    }

    public void depthFirstSearch(int startVertex) {

        Iterator<Integer> vertex = arrLinkedList[startVertex].listIterator();
        visitedArray[startVertex] = true;
        System.out.print(startVertex + " ");
        while (vertex.hasNext()) {
            int n = vertex.next();
            if (!visitedArray[n]) {
                depthFirstSearch(n);
            }
        }
    }

    public static void main(String[] args) {
        DepthFirstSearchGraph graph = new DepthFirstSearchGraph(4);
        graph.addEdge(0,1);
        graph.addEdge(0,2);

        graph.addEdge(1,2);

        graph.addEdge(2,0);
        graph.addEdge(2,3);

        graph.addEdge(3,3);

        graph.printEdge();


        graph.depthFirstSearch(1);
    }


}
