package designpattern.behavioural.visitor;

public class MainCardApplication implements CardApplication {
    @Override
    public String name() {
        return "Main Card Application";
    }

    @Override
    public void accept(final Visitor visitor) {
        visitor.visit(this);

    }
}
