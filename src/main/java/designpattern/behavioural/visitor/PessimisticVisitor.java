package designpattern.behavioural.visitor;

public abstract class PessimisticVisitor implements Visitor {

    @Override
    public void visit(final MainCardApplication mainCardApplication) {
        throw new UnsupportedOperationException("Dont have permission to use Main Card Application");
    }

    @Override
    public void visit(final SupplementaryCardApplication supplementaryCardApplication) {
        throw new UnsupportedOperationException("Dont have permission to use Supplementary Card Application");
    }
}
