package designpattern.behavioural.visitor;

public interface Visitor {

     void visit(MainCardApplication mainCardApplication);

     void visit(SupplementaryCardApplication supplementaryCardApplication);
}
