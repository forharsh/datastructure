package designpattern.behavioural.visitor;

public class Main {
    public static void main(String[] args) {
        CardApplication cardApplication = new MainCardApplication();

        cardApplication.accept(new PessimisticVisitor(){
            @Override
            public void visit(MainCardApplication mainCardApplication) {
                System.out.println(mainCardApplication.name());
            }

            @Override
            public void visit(SupplementaryCardApplication supplementaryCardApplication) {
                System.out.println(supplementaryCardApplication.name());
            }

        });

        cardApplication = new SupplementaryCardApplication();

        cardApplication.accept(new PessimisticVisitor(){
            @Override
            public void visit(MainCardApplication mainCardApplication) {
                System.out.println(mainCardApplication.name());
            }

            @Override
            public void visit(SupplementaryCardApplication supplementaryCardApplication) {
                System.out.println(supplementaryCardApplication.name());
            }

        });
    }
}
