package designpattern.behavioural.visitor;

public class SupplementaryCardApplication implements CardApplication {
    @Override
    public String name() {
        return "Supplementary Card Application.";
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
