package designpattern.behavioural.visitor;

public interface CardApplication {
    String name();

    void accept(Visitor visitor);
}
