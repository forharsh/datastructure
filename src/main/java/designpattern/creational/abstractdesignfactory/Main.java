package designpattern.creational.abstractdesignfactory;

public class Main {
    public static void main(String[] args) {
        Factory factory = new Factory(new WindowUIFactory());
        factory.getButton().paint();
        factory.getCheckBox().paint();

        Factory linuxFactory = new Factory(new LinuxUIFactory());
        linuxFactory.getButton().paint();
        linuxFactory.getCheckBox().paint();

        Factory macFactory = new Factory(new MacUIFactory());
        macFactory.getButton().paint();
        macFactory.getCheckBox().paint();
    }

}
