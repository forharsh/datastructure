package designpattern.creational.abstractdesignfactory;

public class MacBasedCheckbox implements CheckBox {
    @Override
    public void paint() {
        System.out.println("Mac Based Check Box");
    }
}
