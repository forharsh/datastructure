package designpattern.creational.abstractdesignfactory;

public class LinuxUIFactory implements UIAbstractFactory {
    @Override
    public Button createButton() {
        return new LinuxBasedButton();
    }

    @Override
    public CheckBox createCheckBox() {
        return new LinuxBasedCheckBox();
    }
}
