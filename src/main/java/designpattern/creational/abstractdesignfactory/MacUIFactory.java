package designpattern.creational.abstractdesignfactory;

public class MacUIFactory implements  UIAbstractFactory {
    @Override
    public Button createButton() {
        return new MacBasedButton();
    }

    @Override
    public CheckBox createCheckBox() {
        return new MacBasedCheckbox();
    }
}
