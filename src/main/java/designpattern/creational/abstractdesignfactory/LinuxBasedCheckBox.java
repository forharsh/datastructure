package designpattern.creational.abstractdesignfactory;

public class LinuxBasedCheckBox implements CheckBox {
    @Override
    public void paint() {
        System.out.println("linux  Based check box");
    }
}
