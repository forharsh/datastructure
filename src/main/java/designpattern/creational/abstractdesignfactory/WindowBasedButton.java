package designpattern.creational.abstractdesignfactory;

public class WindowBasedButton implements  Button {
    @Override
    public void paint() {
        System.out.println("Window Based Button");
    }
}
