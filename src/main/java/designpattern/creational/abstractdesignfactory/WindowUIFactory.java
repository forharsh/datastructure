package designpattern.creational.abstractdesignfactory;

public class WindowUIFactory implements UIAbstractFactory {

    @Override
    public Button createButton() {
        return new WindowBasedButton();
    }

    @Override
    public CheckBox createCheckBox() {
        return new WindowBasedCheckBox();
    }
}
