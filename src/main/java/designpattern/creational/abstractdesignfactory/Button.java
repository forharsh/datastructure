package designpattern.creational.abstractdesignfactory;

public interface Button {
    void paint();
}
