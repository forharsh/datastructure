package designpattern.creational.abstractdesignfactory;

public class WindowBasedCheckBox implements CheckBox {
    @Override
    public void paint() {
        System.out.println("Window Based check box");
    }
}
