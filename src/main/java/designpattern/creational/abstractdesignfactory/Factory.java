package designpattern.creational.abstractdesignfactory;

public class Factory {

    UIAbstractFactory abstractFactory;

    Factory(UIAbstractFactory abstractFactory) {
        this.abstractFactory = abstractFactory;
    }

    CheckBox getCheckBox() {
        return abstractFactory.createCheckBox();
    }

    Button getButton() {
        return abstractFactory.createButton();
    }
}
