package designpattern.creational.abstractdesignfactory;

public interface CheckBox {
    void paint();

}
