package designpattern.creational.abstractdesignfactory;

public class MacBasedButton implements Button {
    @Override
    public void paint() {
        System.out.println("Mac Based Button");
    }
}
