package designpattern.creational.abstractdesignfactory;

public interface UIAbstractFactory {

    Button createButton();
    CheckBox createCheckBox();
}
