package designpattern.creational.abstractdesignfactory;

public class LinuxBasedButton implements Button {
    @Override
    public void paint() {
        System.out.println("linux  Based button");
    }
}
