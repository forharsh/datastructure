package designpattern.creational.builder;

import java.math.BigDecimal;

public class CardApplication {

    private String embossingLine1;
    private String embossingLine2;
    private String name;
    private BigDecimal annualIncome;
    private Integer noOfHouseHolds;

    private CardApplication() {

    }

    private CardApplication(CardApplicationBuilder builder) {
        this.embossingLine1 = builder.embossingLine1;
        this.embossingLine2 = builder.embossingLine2;
        this.name = builder.name;
        this.annualIncome = builder.annualIncome;
        this.noOfHouseHolds = builder.noOfHouseHolds;
    }

    @Override
    public String toString() {
        return embossingLine1 + "" + embossingLine2 + " " + name + " " + annualIncome + " " + noOfHouseHolds;
    }

    public static class CardApplicationBuilder {
        private String embossingLine1;
        private String embossingLine2;
        private String name;
        private BigDecimal annualIncome;
        private Integer noOfHouseHolds;

        CardApplicationBuilder() {

        }

        public CardApplicationBuilder setEmbossingLine1(final String embossingLine1){
            this.embossingLine1 = embossingLine1;
            return this;
        }

        public CardApplicationBuilder setEmbossingLine2(final String embossingLine2){
            this.embossingLine2 = embossingLine2;
            return this;
        }

        public CardApplicationBuilder setName(final String name){
            this.name = name;
            return this;
        }

        public CardApplicationBuilder setAnnualIncome(final BigDecimal annualIncome){
            this.annualIncome = annualIncome;
            return this;
        }

        public CardApplicationBuilder setNoOfHouseHolds(final Integer noOfHouseHolds){
            this.noOfHouseHolds = noOfHouseHolds;
            return this;
        }

        public  CardApplication build() {
            return new CardApplication(this);
        }

        public static CardApplicationBuilder builder() {
            return new CardApplicationBuilder();
        }
    }
}
