package designpattern.creational.builder;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {

        CardApplication cardApplication = CardApplication.CardApplicationBuilder.builder()
                .setAnnualIncome(new BigDecimal(13232))
                .setNoOfHouseHolds(5)
                .setEmbossingLine1("Harsh")
                .setEmbossingLine2("Vardhan")
                .setName("Khatri").build();

        System.out.println(cardApplication);
    }
}
