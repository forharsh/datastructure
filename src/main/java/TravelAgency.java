// you can also use imports, for example:

import java.util.HashMap;
import java.util.Map;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");
/*
You want to spend your next vacation in a foreign country. In the summer you are free for N consecutive days. You have consulted Travel Agency
and learned that they are offering a trip to some interesting location in the country every day. For simplicity, each location is identified by a number from 0 to N − 1. Trips are described in a non-empty zero-indexed array A: for each K (0 ≤ K < N), A[K] is the identifier of a location which is the destination of a trip offered on dayK. Travel Agency does not have to offer trips to all locations, and can offer more than one trip to some locations.

        You want to go on a trip every day during your vacation. Moreover, you want to visit all locations offered by Travel Agency. You may visit the same location more than once, but you want to minimize duplicate visits. The goal is to find the shortest vacation (a range of consecutive days) that will allow you to visit all the locations offered by Travel Agency.

        For example, consider array A such that:

        A[0] = 7
        A[1] = 3
        A[2] = 7
        A[3] = 3
        A[4] = 1
        A[5] = 3
        A[6] = 4
        A[7] = 1
        Travel Agency offers trips to four different locations (identified by numbers 1, 3, 4 and 7).
        The shortest vacation starting on day 0 that allows you to visit all these locations ends on day 6 (thus is seven days long).
         However, a shorter vacation of five days (starting on day 2 and ending on day 6) also permits you to visit all locations.
          On every vacation shorter than five days, you will have to miss at least one location.

        Write a function:
        int solution(int A[], int N);
        that, given a non-empty zero-indexed array A consisting of N integers, returns the length of the shortest vacation that allows you
         to visit all the offered locations.
        For example, given array A shown above, the function should return 5, as explained above.

        Assume that:

        N is an integer within the range [1..100,000]; each element of array A is an integer within the range [0..N − 1].

        Complexity:

        expected worst-case time complexity is O(N); expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
*/
class TravelAgency {
    public static void main(String[] args) {
        System.out.println(new TravelAgency().solution(new int[]{1, 2, 3, 4, 2, 4, 1, 5}));
    }

    public int solution(int[] A) {
        {
            int arrLength = A.length;
            Map<Integer, Integer> mapCount = new HashMap<>();
            for (int i : A) {
                mapCount.put(i, mapCount.getOrDefault(i, 0) + 1);
            }
            int count = mapCount.size();
            int minVal = arrLength;
            int startIndex = 0;
            int endIndex = 0;
            int freq = 0;
            int[] subArr = new int[arrLength];

            while (endIndex < arrLength) {
                int endVal = A[endIndex];
                if (subArr[endVal] == 0) {
                    freq++;
                }
                subArr[endVal]++;

                if (freq == count) {
                    int startVal = A[startIndex];
                    while (subArr[startVal] > 1) {
                        subArr[startVal]--;
                        startIndex++;
                        startVal = A[startIndex];
                    }
                    minVal = Math.min(minVal, endIndex - startIndex + 1);
                }
                endIndex++;
            }
            return minVal;
        }
    }
}