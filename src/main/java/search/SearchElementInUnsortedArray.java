package search;

public class SearchElementInUnsortedArray {

    public static void main(String[] args) {
        int arr[] = {5, 1, 4, 2, 8};
        SearchElementInUnsortedArray searchElementInUnsortedArray = new SearchElementInUnsortedArray();
        searchElementInUnsortedArray.search(arr, 7);
    }

    // Time Complexity: O(n)
    // Number of Comparisons: Atmost (n+2) comparisons

    private void search(int[] arr, int toSearch) {
        int length = arr.length;
        if (arr[length - 1] == toSearch) {  //Ist comparision
            System.out.println("Element Found");
            return;
        }
        int temp = arr[length - 1];
        arr[length - 1] = toSearch;
        // no termination condition and thus
        // no comparison
        for (int i = 0; ; i++) {
            // this would be executed at-most n times
            // and therefore at-most n comparisons
            if (arr[i] == toSearch) {
                arr[length - 1] = temp;
                if (i < length - 1) {  // 2nd comparision
                    System.out.println("Element Found");
                    return;
                } else {
                    System.out.println("Element Not Found");
                    return;
                }
            }
        }

    }

}
