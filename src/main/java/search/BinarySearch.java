package search;

import sorting.BubbleSort;

public class BinarySearch {

    public static void main(String[] args) {
        int arr[] = {5, 1, 4, 2, 8};
        BinarySearch binarySearch = new BinarySearch();
        BubbleSort.sort(arr, arr.length);
        binarySearch.binarySearch(arr,0,arr.length,3);
    }

    private void binarySearch(int arr[], int left, int right, int toSearch) {
        if(left < right ) {
            int mid = (left + right) / 2;
            if (arr[mid] == toSearch) {
                System.out.println("Element is present");
                return;
            }
            if(toSearch < arr[mid]) {
                binarySearch(arr,left,mid,toSearch);
            }

            if(toSearch > arr[mid]) {
                binarySearch(arr,mid + 1, right, toSearch);
            }
        } else {
            System.out.println("Element is not present");
        }

    }
}
