import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class PrintJob implements  Runnable{
    private String name;
    PrintJob(final String name){
        this.name = name;
    }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " job " + name + " Started");
        try {
            Thread.sleep(5000);
        }  catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " job " + name + " finished");
    }
}
public class Job {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        PrintJob printJob1 = new PrintJob("Harsh");
        PrintJob printJob2 = new PrintJob("Aadira/:/");
        executorService.submit(printJob1);
        executorService.submit(printJob2);
        executorService.shutdown();
    }
}
