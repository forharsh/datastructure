class Nodes {
    int data;
    Nodes next;

    public Nodes() {
    }

    public Nodes(int data, Nodes next) {
        this.data = data;
        this.next = next;
    }
}

public class LinkList {
    Nodes root;
    Nodes head;

    LinkList() {
        root = null;
        head = null;
    }

    public static void main(String[] args) {
        LinkList linkedList = new LinkList();
        linkedList.add(5);
        linkedList.add(1);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(2);
        linkedList.add(1);
        linkedList.add(3, 2);
        System.out.println("After reversing the link list");
        linkedList.print();
        System.out.println("After removing duplicates");
        linkedList.removeDuplicates();
        linkedList.print();
        System.out.println("Sort Link List by Selection Sort");
        linkedList.sort();
        linkedList.print();
        linkedList.middle();
        System.out.println("Reversing the link list");
        linkedList.reverse();
        linkedList.print();
        System.out.println("Delete node having value : 2");
        linkedList.delete(2);
        linkedList.print();
    }

    private void delete(int toDeleteData) {
        Nodes head = root;
        Nodes prev = null;
        if( head == null ) {
            return;
        }
        if(head.data == toDeleteData) {
            root = head.next;
            return;
        }
        while(head != null && head.data != toDeleteData){
              prev = head;
              head = head.next;
        }
        prev.next = head.next;
    }

    private void sort() {
        Nodes ptr1 = root;
        if (head == null) {
            return;
        }
        while (ptr1 != null) {
            Nodes ptr2 = ptr1;
            while (ptr2.next != null) {
                if (ptr1.data > ptr2.next.data) {
                    int temp = ptr1.data;
                    ptr1.data = ptr2.next.data;
                    ptr2.next.data = temp;
                }
                ptr2 = ptr2.next;
            }
            ptr1 = ptr1.next;
        }
    }

    private void reverse() {
       Nodes current = root;
       Nodes next,prev = null;
       while(current != null) {
           next = current.next;
           current.next = prev;
           prev = current;
           current = next;
       }
       root = prev;
    }

    private void middle() {
        if(root == null) {
            return;
        }
        Nodes slowPointer = root;
        Nodes fastPointer = root;
        while (fastPointer != null && fastPointer.next != null) {
            fastPointer = fastPointer.next.next;
            slowPointer = slowPointer.next;
        }
        System.out.println("Middle of the linklist is : " + slowPointer.data);
    }

    private void add(int data) {
        if (root == null) {
            root = new Nodes(data, null);
            head = root;
        } else {
            Nodes node = new Nodes(data, null);
            head.next = node;
            head = node;
        }
    }

    private void add(int data, int pos) {
        if (root == null) {
            root = new Nodes(data, null);
            head = root;
        } else {
            Nodes node = new Nodes(data, null);
            head.next = node;
            head = node;
        }
    }

    public void print() {
        head = root;
        System.out.print(" HEAD ===>> ");
        while (head != null) {
            System.out.print(head.data + " ====>> ");
            if (head.next == null) {
                break;
            } else {
                head = head.next;
            }
        }
        System.out.println("NULL");
    }

    void removeDuplicates() {
        Nodes ptr1 = root;
        if (head == null) {
            return;
        }
        while (ptr1 != null) {
            Nodes ptr2 = ptr1;
            while (ptr2.next != null) {
                if (ptr1.data == ptr2.next.data) {
                    ptr2.next = ptr2.next.next;
                } else {
                    ptr2 = ptr2.next;
                }
            }
            ptr1 = ptr1.next;
        }
    }


}
