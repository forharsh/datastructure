package file;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Collectors;

class Employee {
    private Integer id;
    private String name;
    Employee(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        Employee employee = (Employee) obj;
        return Objects.equals(this.name,employee.name);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
public class FileHandling {
    public static void main(String[] args) throws IOException, InterruptedException {

        List<Employee> listAll = Arrays.asList(new Employee(1, "Harsh"), new Employee(2, "Harsh"), new Employee(1, "Vardhan"));
        listAll = listAll.stream().distinct().collect(Collectors.toList());
        System.out.println(listAll);
         /*Files
                .list(Paths.get("D:\\Backup\\Harsh docs\\FINLAND\\HARSH"))
                .filter(path -> path.toString().endsWith(".txt"))
                .forEach(path1 -> {
                    try {
                        listAll.addAll(Files.lines(path1).map(str -> {
                            String[] splittedString = str.split("[|]");
                            FlightSearchDTO flightSearchDTO = new FlightSearchDTO();
                            flightSearchDTO.setName1(splittedString[0]);
                            flightSearchDTO.setName2(splittedString[1]);
                            flightSearchDTO.setName3(splittedString[2]);
                            flightSearchDTO.setName4(splittedString[3]);
                            return flightSearchDTO;
                        }).collect(Collectors.toList()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        listAll.forEach(System.out::println);
        Files.write(Paths.get("D:\\Backup\\Harsh docs\\FINLAND\\HARSH\\output.txt"), "Harsh".getBytes());
*/
        /*ExecutorService executorService = Executors.newFixedThreadPool(5);
        executorService.submit(() -> {

        });
        executorService.shutdown();*/

        CountDownLatch countDownLatch = new CountDownLatch(2);
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(i);
            }
            countDownLatch.countDown();
            System.out.println("Count down");
        });
        t1.start();
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(i);
            }
            countDownLatch.countDown();
        });
        t2.start();
        countDownLatch.await();
        System.out.println("Main Thread ends........");


        CyclicBarrier cyclicBarrier = new CyclicBarrier(2);
        Thread t3 = new Thread(() -> {
            for(int i = 0 ; i < 10 ; i++) {
                System.out.println(i);
            }
            try {
                cyclicBarrier.await();

                System.out.println("Thread t3 is done after waiting");
            }catch (BrokenBarrierException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t3.start();

        Thread t4 = new Thread(() -> {
            for(int i = 0 ; i < 10 ; i++) {
                System.out.println(i);
            }
            try {
                cyclicBarrier.await();
                System.out.println("Thread t4 is done after waiting");
            }catch (BrokenBarrierException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t4.start();
        System.out.println("Main thread ends with cyclic barrier");
        System.out.println("sfsd " + ( 6 - 2 + 10 % 4 + 7));
    }
}


