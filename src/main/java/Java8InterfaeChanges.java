interface Interface1 {

    void method1(String str);

    default void log(String str){
        System.out.println("I1 logging::"+str);
    }

}

interface Interface2 {

    void method2();

   /* default void log(String str){
        System.out.println("I2 logging::"+str);
    }*/

}


public class Java8InterfaeChanges implements Interface1, Interface2 {

    @Override
    public void method2() {
    }

    @Override
    public void method1(String str) {
    }

  /*  @Override
    public void log(String str) {
        System.out.println("Hello from concrete class");
    }*/

    public static void main(String[] args) {
        Java8InterfaeChanges abc = new Java8InterfaeChanges();
        abc.log("asdf");
        Interface1 interface1 = abc;
        interface1.log("interface");

    }
}
