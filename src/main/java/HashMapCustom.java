/**
 * Custom Hash Map Implementation.
 *
 * @author harshvardhan
 * @param <K>
 * @param <V>
 */
public class HashMapCustom<K, V> {

    Entry<K, V>[] table;
    int capacity = 4;

    public HashMapCustom() {
        table = new Entry[capacity];
    }

    public static void main(String[] args) {
        HashMapCustom<String, String> hashMapCustom = new HashMapCustom();
        hashMapCustom.put("Harsh", "Vardhan");
        hashMapCustom.put("Harsh", "Khatri");
        hashMapCustom.put("Aadira", "Khatri");
        System.out.println(hashMapCustom.get("Harsh"));
        System.out.println(hashMapCustom.get("Aadira"));
        hashMapCustom.remove("Aadira");
        System.out.println("After removing");
        System.out.println(hashMapCustom.get("Harsh"));
        System.out.println(hashMapCustom.get("Aadira"));
    }

    private void remove(K key) {
        int hash = hash(key);
        Entry<K, V> current = table[hash];
        Entry<K, V> previous = null;
        while (current != null) {
            if (current.key.equals(key)) {
                if (previous == null) {
                    table[hash] = table[hash].next;
                } else {
                    previous.next = current.next;
                    return;
                }
            }
            previous = current;
            current = current.next;
        }
    }

    private V get(K key) {
        int hash = hash(key);
        Entry<K, V> current = table[hash];
        if (current == null) {
            return null;
        }
        while (current != null) {
            if (current.key.equals(key)) {
                return current.value;
            }
            current = current.next;
        }
        return null;
    }

    private void put(K key, V value) {
        if (key == null) { // Key should not be null;
            return;
        }
        int hash = hash(key);
        Entry<K, V> current = table[hash];
        Entry<K, V> newEntry = new Entry<>(key, value, null);
        Entry<K, V> previous = null;
        if (current == null) {
            table[hash] = newEntry;
        } else { // It means collision occurs.
            while (current != null) {
                if (current.key.equals(key)) {
                    if (previous == null) {
                        newEntry.next = current.next;
                        table[hash] = newEntry;
                    } else {
                        previous.next = newEntry;
                        newEntry.next = current.next;
                    }

                }
                previous = current;
                current = current.next;
            }
            previous.next = newEntry; // It means keys are not equals in linked list.
        }
    }

    private int hash(K key) {
        return Math.abs(key.hashCode()) % capacity;
    }

    static class Entry<K, V> {
        K key;
        V value;
        Entry<K, V> next;

        public Entry(K key, V value, Entry<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
